package ee.rustam.bwt;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import com.google.android.material.snackbar.Snackbar;

import java.time.LocalDateTime;

import ee.rustam.bwt.databinding.ActivityMainBinding;
import ee.rustam.bwt.model.Record;
import ee.rustam.bwt.model.RecordDatabase;

public class MainActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		db = Room.databaseBuilder(getApplicationContext(), RecordDatabase.class, "default-db")
				.fallbackToDestructiveMigration()
				.build();

		binding = ActivityMainBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		setSupportActionBar(binding.toolbar);

		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
		appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
		NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

		binding.fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				var weightInput = (EditText) findViewById(R.id.editWeightMeasurement);
				var dateInput = (EditText) findViewById(R.id.editDateOfMeasurement);
				var timeInput = (EditText) findViewById(R.id.editTimeOfMeasurement);
				var newRecord = new Record(
						Double.parseDouble(String.valueOf(weightInput.getText())),
						LocalDateTime.parse(String.format("%sT%s", dateInput.getText().toString(), timeInput.getText().toString())));
				new Thread(() ->
				{
					db.recordDao().insert(newRecord);
					Snackbar.make(view, "Record was saved", Snackbar.LENGTH_LONG)
							.setAction("Action", null).show();
				}).start();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onSupportNavigateUp() {
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
		return NavigationUI.navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp();
	}

	private AppBarConfiguration appBarConfiguration;
	private ActivityMainBinding binding;
	private RecordDatabase db;
}