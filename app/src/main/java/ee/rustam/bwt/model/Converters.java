package ee.rustam.bwt.model;

import androidx.room.TypeConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class Converters
{
	@TypeConverter
	public static LocalDateTime fromTimestamp(Long value)
	{
		if (value == null) return null;

		var instant = Instant.ofEpochSecond(value);
		return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
	}

	@TypeConverter
	public static Long toTimestamp(LocalDateTime value)
	{
		if (value == null) return null;

		return value.toEpochSecond(ZoneOffset.UTC);
	}
}
