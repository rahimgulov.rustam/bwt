package ee.rustam.bwt.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RecordDao
{
	@Query("SELECT * FROM record ORDER BY measurementTimestamp DESC")
	List<Record> getAll();

	@Insert
	void insert(Record record);

	@Delete
	void delete(Record record);
}
