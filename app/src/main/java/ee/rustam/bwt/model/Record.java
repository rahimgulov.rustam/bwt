package ee.rustam.bwt.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.LocalDateTime;

@Entity
public class Record
{
	public Record(double weight, LocalDateTime measurementTimestamp)
	{
		this.weight = weight;
		this.measurementTimestamp = measurementTimestamp;
	}
	@PrimaryKey(autoGenerate = true)
	public int id;

	@ColumnInfo
	public LocalDateTime measurementTimestamp;

	@ColumnInfo
	public double weight;
}
