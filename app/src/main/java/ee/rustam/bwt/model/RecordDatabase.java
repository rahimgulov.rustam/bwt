package ee.rustam.bwt.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(
		entities = {Record.class},
		version = 2
		//autoMigrations = { @AutoMigration(from = 1, to = 2)}
)
@TypeConverters({Converters.class})
public abstract class RecordDatabase extends RoomDatabase
{
	public abstract RecordDao recordDao();
}
