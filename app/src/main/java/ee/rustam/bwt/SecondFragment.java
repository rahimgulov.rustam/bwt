package ee.rustam.bwt;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import ee.rustam.bwt.databinding.FragmentSecondBinding;
import ee.rustam.bwt.model.RecordDatabase;

public class SecondFragment extends Fragment
{
	private FragmentSecondBinding binding;

	@Override
	public View onCreateView(
			LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState
	)
	{
		binding = FragmentSecondBinding.inflate(inflater, container, false);
		var context = requireContext().getApplicationContext();
		var db = Room.databaseBuilder(context, RecordDatabase.class, "default-db")
				.fallbackToDestructiveMigration()
				.build();


		var recyclerView = (RecyclerView) binding.getRoot().findViewById(R.id.recyclerview);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		recyclerView.setAdapter(new RecordAdapter(new String[]{"data1", "data2", "data3"}));

		new Thread(() -> {
			var all = db.recordDao().getAll();
			var data = (String[]) all
					.stream()
					.map(it -> String.format("%f @ %s", it.weight, it.measurementTimestamp))
					.toArray(String[]::new);
			recyclerView.setAdapter(new RecordAdapter(data));
		}).start();
		return binding.getRoot();
	}

	public void onViewCreated(@NonNull View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		binding.buttonSecond.setOnClickListener(view1 -> NavHostFragment.findNavController(SecondFragment.this)
				.navigate(R.id.action_SecondFragment_to_FirstFragment));
	}

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		binding = null;
	}
}